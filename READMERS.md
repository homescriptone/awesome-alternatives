# Ресурсы сети

A curated list of mostly [*free* and open source](https://en.wikipedia.org/wiki/Free_and_open-source_software) alternatives to [proprietary software](https://en.wikipedia.org/wiki/Proprietary_software) and services. Some of the software below has proprietary backend, and only the application or the frontend the user interacts with is open source. 

## Legend:

### [CATEGORY]

#### [NAME OF FREE AND OPEN SOURCE SERVICE] (LINK)

*Alternative to [PROPRIETARY SERVICE(S)]* — [DESCRIPTION]

## [Social media](https://en.wikipedia.org/wiki/Social_media)

### [PeerTube](https://joinpeertube.org/instances) [![Generic badge](https://img.shields.io/badge/Federated-brightgreen.svg)](https://img.shields.io/badge/Federated-brightgreen.svg) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg)

*Alternative to YouTube* — PeerTube is a free and open-source, [decentralized](https://en.wikipedia.org/wiki/Decentralization), federated video platform, powered by [ActivityPub](https://en.wikipedia.org/wiki/ActivityPub) and [WebTorrent](https://en.wikipedia.org/wiki/WebTorrent), that uses [peer-to-peer](https://en.wikipedia.org/wiki/Peer-to-peer) technology to reduce load on individual servers when viewing videos. You can like, comment, download videos, subscribe to channels, create playlists all for free, as well as create your own instance.

*Text from [Wikipedia](https://en.wikipedia.org/wiki/PeerTube).*

### [Mastodon](https://joinmastodon.org) [![Generic badge](https://img.shields.io/badge/Federated-brightgreen.svg)](https://img.shields.io/badge/Federated-brightgreen.svg) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg)

*Alternative to Twitter* — Mastodon is a free and open-source self-hosted social networking service. It allows anyone to host their own server node in the network, and its various separately operated user bases are federated across many different servers.

*Text from [Wikipedia](https://en.wikipedia.org/wiki/Mastodon_(software)).*

### [Lemmy](https://dev.lemmy.ml/) [![Generic badge](https://img.shields.io/badge/Federated-brightgreen.svg)](https://img.shields.io/badge/Federated-brightgreen.svg) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg)

*Alternative to Reddit* — Lemmy is similar to sites like Reddit, Lobste.rs, Raddle, or Hacker News: you subscribe to forums you're interested in, post links and discussions, then vote, and comment on them. Behind the scenes, it is very different; anyone can easily run a server, and all these servers are federated (think email), and connected to the same universe, called the Fediverse.

*Text from [GitHub](https://github.com/LemmyNet/lemmy).*

### [Pixelfed](https://pixelfed.social/) [![Generic badge](https://img.shields.io/badge/Federated-brightgreen.svg)](https://img.shields.io/badge/Federated-brightgreen.svg) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg)

*Alternative to Instagram* — A free and ethical photo sharing platform, powered by ActivityPub federation.

*Text from [GitHub](https://github.com/pixelfed/pixelfed).*

### [Friendica](https://friendi.ca/) [![Generic badge](https://img.shields.io/badge/Federated-brightgreen.svg)](https://img.shields.io/badge/Federated-brightgreen.svg) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg)

*Alternative to Facebook* — Friendica is an open, free, distributed social network. It forms one part of the Fediverse, an interconnected and decentralized network of independently operated servers. 

*Text from [Wikipedia](https://en.wikipedia.org/wiki/Friendica).*

## [Communication Platforms](https://en.wikipedia.org/wiki/Internet_Relay_Chat)

### [Matrix](https://matrix.org/) [![Generic badge](https://img.shields.io/badge/Federated-brightgreen.svg)](https://img.shields.io/badge/Federated-brightgreen.svg) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg)

*Alternative to Discord* — Matrix is an open standard and lightweight protocol for real-time communication. It is designed to allow users with accounts at one communications service provider to communicate with users of a different service provider via online chat, voice over IP, and videotelephony. The most popular way to interact with matrix is through [Riot](https://riot.im/)

### [Mattermost](https://mattermost.com/) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg)

*Alternative to Slack* — Mattermost is an open-source, self-hostable online chat service with file sharing, search, and integrations. It is designed as an internal chat for organisations and companies, and mostly markets itself as an open-source alternative to Slack and Microsoft Teams. 

*Text from [Wikipedia](https://en.wikipedia.org/wiki/Mattermost).*

### [zulipchat](https://zulipchat.com/) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg)

*Alternative to Slack* — In Zulip, communication occurs in streams (which are like channels in IRC). Each stream can have several topics - Zulip features a unique threading model, in which each message also has a topic, along with the content. Zulip claims that this improves productivity by "making easy to catch up after a day of meetings". Apart from this, Zulip offers standard features found in collaboration apps like message reactions, message search history, polls, private messaging, group messaging etc. Zulip streams can be private or public.

*Text from [Wikipedia](https://en.wikipedia.org/wiki/Zulip).*

### [Signal](https://signal.org/en/) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg) **Require a phone number**

*Alternative to WhatsApp/SMS* — Signal is a cross-platform encrypted messaging service developed by the Signal Foundation and Signal Messenger LLC. It uses the Internet to send one-to-one and group messages, which can include files, voice notes, images and videos. Its mobile apps can also make one-to-one voice and video calls 

*Text from [Wikipedia](https://en.wikipedia.org/wiki/Signal_(software)).*

### [Session](https://getsession.org/) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg) **Does not require a phone number**

*Alternative to WhatsApp* — Session masters the usual chat functions. There are voice messages, a GIF search (with privacy warning), file sharing and group chats. You can add new contacts by scanning a QR code or exchanging the session ID. You can share your groups via a link. Another advantage over Signal or Telegram is end-to-end encrypted group chats. Up to ten people can network fully anonymously via session. Yes, WhatsApp also has encrypted group chats. But your metadata, phone numbers, and IP addresses remain visible to Facebook. Session is completely unsuspecting.

*Text from [AndroidPIT](https://www.androidpit.com/session-messenger-review).*

### [Briar](https://code.briarproject.org/briar/briar) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg)

*Alternative to WhatsApp* — Briar is an open-source software communication technology, intended to provide secure and resilient peer-to-peer communications with no centralized servers and minimal reliance on external infrastructure. Connections are made through bluetooth, WiFi, or over the internet via Tor and all private communication is end-to-end encrypted.

*Text from [Wikipedia](https://en.wikipedia.org/wiki/Briar_(software)).*

### [Jitsi](https://jitsi.org) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg)

*Alternative to Zoom* — Jitsi is a collection of free and open-source multiplatform voice (VoIP), videoconferencing and instant messaging applications for the web platform, Windows, Linux, macOS, iOS and Android.

*Text from [Wikipedia](https://en.wikipedia.org/wiki/Jitsi).*

### [tox](https://tox.chat/) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg) **Does not require a registration**

Tox is a peer-to-peer instant-messaging and video-calling protocol that offers end-to-end encryption. The stated goal of the project is to provide secure yet easily accessible communication for everyone.

*Text from [Wikipedia](https://en.wikipedia.org/wiki/Tox_(protocol))*

### [retroshare](https://retroshare.cc/) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg) **Does not require a registration**

RetroShare is a free and open-source peer-to-peer communication and file sharing app based on a friend-to-friend network built on GNU Privacy Guard (GPG). Optionally, peers may communicate certificates and IP addresses from and to their friends.

*Text from [Wikipedia](https://en.wikipedia.org/wiki/RetroShare)*

### [Jami](https://jami.net/) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg) **Does not require a registration**

Jami (formerly GNU Ring, SFLphone) is a SIP-compatible distributed peer-to-peer softphone and SIP-based instant messenger for Linux, Microsoft Windows, OS X, iOS, and Android.

*Text from [Wikipedia](https://en.wikipedia.org/wiki/Jami_(software))*

### [Ricochet](https://ricochet.im/) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg) **Does not require a registration**  
[Ricochet Refresh](https://ricochetrefresh.net/)

Ricochet or Ricochet IM is a free software, multi-platform, instant messaging software project originally developed by John Brooks[4] and later adopted as the official instant messaging client project of the Invisible.im group.[5] A goal of the Invisible.im group is to help people maintain privacy by developing a "metadata free" instant messaging client.

*Text from [Wikipedia](https://en.wikipedia.org/wiki/Ricochet_(software))*

## Miscellaneous services

### [Nextcloud](https://nextcloud.com/) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg)

*Alternative to Google Drive/Dropbox* — Nextcloud is a suite of client-server software for creating and using file hosting services. Nextcloud is free and open-source, which means that anyone is allowed to install and operate it on their own private server devices.

Nextcloud application functionally is similar to Dropbox, Office 365 or Google Drive, but can be used on home-local computers or for off-premises file storage hosting. Office functionality is limited to x86/x64 based servers as OnlyOffice does not support ARM processors. In contrast to proprietary services the open architecture enables users to have full control of their data.

*Text from [Wikipedia](https://en.wikipedia.org/wiki/Nextcloud).*

### [CryptPad](https://cryptpad.fr/) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg)

*Alternative to Google Docs* — CryptPad is a private-by-design alternative to popular office tools and cloud services. All the content stored on CryptPad is encrypted before being sent

*Text from [CryptPad](https://cryptpad.fr/).*

### [LimeSurvey](https://www.limesurvey.org/) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg)

*Alternative to Google Forms* — is a free and open source on-line statistical survey web app written in PHP based on a MySQL, SQLite, PostgreSQL or MSSQL database, distributed under the GNU General Public License. As a web server-based software it enables users using a web interface to develop and publish on-line surveys, collect responses, create statistics, and export the resulting data to other applications. 

*Text from [Wikipedia](https://en.wikipedia.org/wiki/LimeSurvey).*

### [AuroraOSS](https://auroraoss.com/) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg)

*Alternative to Google Play* — Aurora Store is an Unofficial FOSS client to Google's Play Store

*Text from [AuroraOSS](https://auroraoss.com/).*

### [F-Droid](https://f-droid.org/) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg)

*[OSS](https://en.wikipedia.org/wiki/Open-source_software) Alternative to Google Play* — F-Droid is a community-maintained software repository for Android, similar to the Google Play store. The main repository, hosted by the project, contains only free/libre apps.

*Text from [Wikipedia](https://en.wikipedia.org/wiki/F-Droid).*

### [Searx](https://en.wikipedia.org/wiki/Searx) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg)

*Alternative to Google Search* — searx is a free metasearch engine, available under the GNU Affero General Public License version 3, with the aim of protecting the privacy of its users. To this end, searx does not share users' IP addresses or search history with the search engines from which it gathers results. Tracking cookies served by the search engines are blocked

**Main Searx Instances**
https://searx.me/
https://searx.be/
https://searx.ninja/

*Text from [Wikipedia](https://en.wikipedia.org/wiki/Searx).*

### [LibreOffice](https://www.libreoffice.org/) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg) & [OnlyOffice Community Edition](https://www.onlyoffice.com/) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg)

*Alternatives to Microsoft Office* — LibreOffice: LibreOffice is a free and open-source office suite, a project of The Document Foundation. 

*Text from [Wikipedia](https://en.wikipedia.org/wiki/LibreOffice).*

OnlyOffice: ONLYOFFICE is a project developed by experienced IT experts from Ascensio System SIA, leading IT company with headquarters in Riga, Latvia. Originally ONLYOFFICE was designed for internal team collaboration.

*Text from [OnlyOffice](https://www.onlyoffice.com/about.aspx).*


### [GIMP](https://www.gimp.org/) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg) & [Krita](https://krita.org/en/) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg)

*Alternatives to Photoshop* — GIMP: GNU Image Manipulation Program is a free and open-source raster graphics editor used for image retouching and editing, free-form drawing, converting between different image formats, and more specialized tasks. 

*Text from [Wikipedia](https://en.wikipedia.org/wiki/GIMP).*

Krita: Krita is a free and open-source raster graphics editor designed primarily for digital painting and 2D animation. It features an OpenGL-accelerated canvas, colour management support, an advanced brush engine, non-destructive layers and masks (similar to Adobe Photoshop), group-based layer management, vector artwork support and switchable customisation profiles. 

*Text from [Wikipedia](https://en.wikipedia.org/wiki/Krita)*

### [Inkscape](https://inkscape.org/) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg)

*Alternatives to Adobe Illiustrator* — Inkscape is a free and open-source vector graphics editor. Inkscape's primary vector graphics format is Scalable Vector Graphics (SVG); other formats can be imported and exported.

*Text from [Wikipedia](https://en.wikipedia.org/wiki/Inkscape).*

### [BitWarden](https://bitwarden.com/) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg) & [KeePassXC](https://keepassxc.org/) [![Generic badge](https://img.shields.io/badge/FOSS-OSS-blue.svg)](https://img.shields.io/badge/FOSS-OSS-blue.svg)

*Alternatives to Proprietary Password Managers* — Bitwarden: Bitwarden is a free and open-source password management service that stores sensitive information such as website credentials in an encrypted vault. The Bitwarden platform offers a variety of client applications including a web interface, desktop applications, browser extensions, mobile apps, and a CLI.

*Text from [Wikipedia](https://en.wikipedia.org/wiki/Bitwarden).*

KeePassXC: The Electronic Frontier Foundation mention KeePassXC as "an example of a password manager that is open-source and free."The tech collective PrivacyTools has included KeePassXC in their list of recommended password manager software because of its active development.

*Text from [Wikipedia](https://en.wikipedia.org/wiki/KeePassXC).*

## External sources

### [samedamci / FOSS_Stuff](https://git.samedamci.me/samedamci/FOSS_Stuff)

A more in-depth list of free and open source software ranging from Android, to Windows, to Linux with a bonus of some tips and tricks!

Mirrors in: *[GitHub](https://github.com/samedamci/FOSS_Stuff)* and *[GitLab](https://gitlab.com/samedamci/FOSS_Stuff).*